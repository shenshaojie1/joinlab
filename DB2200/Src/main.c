/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "ht1623.h"
#include "lcd.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
void speed_deal(void);
void status_up(void);
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
uint16_t Wise_Flag;//反转标志
uint16_t UP_Flag;//正转标志
uint32_t Set_Cnt1;//旋钮转速计数
uint32_t speed_set1;//旋钮转速
uint32_t next_cnt1;//旋钮时钟时间
uint32_t Set_Cnt2;//旋钮转速计数
uint32_t speed_set2;//旋钮转速
uint32_t next_cnt2;//旋钮时钟时间
uint8_t ADD_Mode;//显示增减模式
uint8_t  wise_blink=0;//运行闪烁
uint8_t time_disable;//关闭计时
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
const uint16_t  tempRes_buf[181] = {   3876, 3863, 3849, 3835, 3819, 3804, 3787, 
3769, 3751, 3732, 3712, 3692, 3670, 3648, 3625, 3601, 3576, 3550, 3523, 3496,   
3467, 3438, 3408, 3377, 3344, 3312, 3278, 3243, 3208, 3171, 3134, 3096, 3057, 
3018, 2978, 2937, 2896, 2854, 2811, 2768,   2725, 2681, 2637, 2592, 2547, 2502, 
2457, 2411, 2366, 2320, 2274, 2229, 2183, 2138, 2092, 2048, 2003, 1958, 1914, 
1871,   1827, 1784, 1742, 1700, 1659, 1618, 1578, 1538, 1499, 1461, 1423, 1386, 
1350, 1314, 1279, 1245, 1211, 1179, 1146, 1115,   1084, 1054, 1025, 996, 968, 941, 
915, 889, 864, 839, 815, 792, 769, 747, 726, 705, 685, 665, 646, 628, 610, 592, 
575,   558, 542, 527, 512, 497, 483, 469, 456, 443, 430, 418, 406, 395, 383, 373, 
362, 352, 342, 333, 323, 314, 306, 297,   289, 281, 273, 266, 259, 252, 245, 238, 
232, 225, 219, 214, 208, 202, 197, 192, 187, 182, 177, 173, 168, 164, 160,   155, 
152, 148, 144, 140, 137, 133, 130, 127, 124, 121, 118, 115, 112, 109, 107, 104, 
102, 99, 97, 94, 92, 90, 88,  86, 84, 82, 80, 78, 76, 75, 73}; //-30℃到180℃
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
	
	Sys_Init();
  HAL_GPIO_WritePin(GPIOA, LED_ADJ_Pin, GPIO_PIN_RESET);
	lcd_init();
	HAL_Delay (5);
  lcd_all();
	HAL_Delay(1000);
	lcd_clr();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		LCD_Display();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL4;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
/**
  * @brief ?????,??AD?????
  * @param AD?
  * @Note  ?????-30~150? ??????,??32767 ??10K??,3950 10K????
  * @retval ???
  */  
short calcuTem(uint16_t  ad_value)
{
  short tempValue= 0x7fff;
  if ((ad_value < 3877)&&(ad_value > 72))
  {
    for (short i = 0 ; i < 181 ; i++)
    {
      if (ad_value > tempRes_buf[i])
      {
        tempValue = i-30;
        break;
      }
    }
  }
  //else err return 0x7fff
  return tempValue;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
